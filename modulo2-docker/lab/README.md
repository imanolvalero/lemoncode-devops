# Modulo 2 - Contenedores Docker - Laboratorio

### Ejercicio 1
[[Ir a la solucion]](./main/modulo2-docker/lab/ejercicio1.sh)

Dockeriza la aplicación dentro de [lemoncode-challenge](https://github.com/Lemoncode/bootcamp-devops-lemoncode/tree/master/01-contenedores/lemoncode-challenge), la cual está compuesta de 3 partes:
* Un front-end con `Node.js`
* Un backend en `.NET` que utiliza un MongoDB para almacenar la información.
* El `MongoDB` donde se almacena la información en una base de datos.

Requisitos del ejercicio:
1. Los tres componentes deben estar en una red llamada lemoncode-challenge.
1. El backend debe comunicarse con el `mongodb` a través de esta URL `mongodb://some-mongo:27017`
1. El front-end debe comunicarse con la api a través de `http://topics-api:5000/api/topics`
1. El front-end debe estar mapeado con el host para ser accesible a través del puerto __8080__.
1. El `MongoDB` debe almacenar la información que va generando en un __volumen__, mapeado a la ruta `/data/db`.
1. Este debe de tener una base de datos llamada `TopicstoreDb` con una colección llamada `Topics`. La colección `Topics` debe tener esta estructura: `{"_id":{"$oid":"5fa2ca6abe7a379ec4234883"},"Name":"Contenedores"}` ¡Añade varios registros!

Tip para backend: Antes de intentar contenerizar y llevar a cabo todos los pasos del ejercicio se recomienda intentar ejecutar la aplicación sin hacer cambios en ella. En este caso, lo único que es posible que “no tengamos a mano” es el `MongoDB`. Por lo que empieza por crear este en Docker, usa un cliente como el que vimos en el primer día de clase (`MongoDB Compass`) para añadir datos que pueda devolver la API.
Nota: Se ha reemplazado `Mongo Compass`, que es una aplicación de escritorio, por `Mongo Express`, que es una aplicacion web. La razón es que es más sencillo de testear y se consigue el mismo objetivo.

![Mongo compass](../assets/lab-mongo.png)

Nota: es más fácil si abres __Visual Studio Code__ desde la carpeta backend para hacer las pruebas y las modificaciones que si te abres desde la raíz del repo. Para ejecutar este código solo debes lanzar dotnet run

Tip para frontend: Para ejecutar el frontend abre esta carpeta en __VS Code__ y ejecuta primero `npm install`. Una vez instaladas las dependencias ya puedes ejecutarla con `npm start`. Debería de abrirse un navegador con lo siguiente:

#### Topics
```
Contenedores
```

### Ejercicio 2
[[Ir a la solucion]](./main/modulo2-docker/lab/ejercicio2.yaml)

Ahora que ya tienes la aplicación del ejercicio 1 dockerizada, utiliza Docker Compose para lanzar todas las piezas a través de este. Debes plasmar todo lo necesario para que esta funcione como se espera: la red que utilizan, el volumen que necesita MongoDB, las variables de entorno, el puerto que expone la web y la API. Además debes indicar qué comandos utilizarías para levantar el entorno, pararlo y eliminarlo.
