#!/bin/bash
IFS=$'\n'

REPO_URL=https://github.com/Lemoncode/bootcamp-devops-lemoncode
TIMESTAMP=$(date "+%Y%m%d%H%M%S")

NERTWORK_NAME=lemoncode-challenge
PORT=8080

DBSERVER_NAME=some-mongo
DBSERVER_BASE=mongo:latest
DBSERVER_PORT=27017
DBSERVER_USER=root
DBSERVER_PASS=pass
DBSERVER_VOLUME=${DBSERVER_NAME}-volume
DBSERVER_URL=mongodb://${DBSERVER_USER}:${DBSERVER_PASS}@${DBSERVER_NAME}:${DBSERVER_PORT}

DBSERVER_TEST_NAME=${DBSERVER_NAME}-test
DBSERVER_TEST_BASE=mongo-express:latest
DBSERVER_TEST_PORT=8081

BACKEND_NAME=topics-api
BACKEND_PORT=5000
BACKEND_BASE=mcr.microsoft.com/dotnet/aspnet:3.1-focal
BACKEND_BUILD=mcr.microsoft.com/dotnet/sdk:3.1-focal 
BACKEND_REPO=01-contenedores/lemoncode-challenge/backend

BACKEND_TEST_NAME=${BACKEND_NAME}-test
BACKEND_TEST_BASE=alpine:latest

FRONTEND_NAME=frontend
FRONTEND_BASE=node:17-alpine3.13
FRONTEND_PORT=3000
FRONTEND_REPO=01-contenedores/lemoncode-challenge/frontend


function cloneGitSubfolder(){
    local repo="$1"
    local rdir="$2"
    local dest="$3"
    local cdir="${PWD}"
    local temp=$(mktemp -d -t cloneGitSubFolder.XXXXX)
    
    cd ${temp}
    git init &> /dev/null
    git remote add -f origin ${repo} &> /dev/null
    git config core.sparseCheckout true &> /dev/null
    echo "${rdir}" >> .git/info/sparse-checkout
    git pull origin master &> /dev/null
    cd ${cdir}
    rsync -r -f "-x .git" ${temp}/${rdir}/* ${dest}
    rm -Rf ${temp}
    return 0
}

function simpleTest(){
    local name="${1:-"Simple test"}"
    local desc="${2:-""}"
    local url=${3:-http://localhost}
    local text="${4:-""}"
    local delay="${5:-0}"
    local custom="${6:-""}"
    
    echo -e "\e[1mSimple Test: ${name}\e[0m\n$desc"
    [[ ${delay} -gt 0 ]] && (
        local bar=""
        local count=0
        while [ ${count} -lt ${delay} ]; do
            bar=""            
            local i=0
            while [ ${i} -lt ${delay} ]; do
                [[ ${i} -gt count ]] && bar="${bar} " || bar="${bar}>"
                (( i++ ))
            done
            echo -ne "\e[33mWaiting [${bar}] \e[0m\r"
            (( count++ ))
            sleep 1
        done
    )
    echo -ne "\e[33mTesting...\e[0m                                          \r"
    local cmd="curl -s ${url}"
    [[ -n "${custom}" ]] && cmd="${custom}"
    [[ $(eval ${cmd} | grep -c "${text}") -eq 1 ]] \
        && (echo -e "\e[32mTest passed\e[0m\n" && return 0) \
        || (echo -e "\e[31mTest failed\e[0m\n" && return 1)
}


# NETWORK ==========================================================================================
[ $(docker network ls | grep -c ${NERTWORK_NAME}) -eq 0 ] && docker network create ${NERTWORK_NAME}

# MONGODB ==========================================================================================
# Stop container if it is running
[ $(docker ps -a | grep -c ${DBSERVER_NAME}) -eq 1 ] && \
    docker stop ${DBSERVER_NAME}

# Create volume if not exists
[ $(docker volume ls | grep -c ${DBSERVER_VOLUME}) -eq 1 ] && \
    docker volume rm ${DBSERVER_VOLUME}
docker volume create ${DBSERVER_VOLUME}

# Launch DBServer
docker run --detach --rm \
    --name ${DBSERVER_NAME} \
    --network ${NERTWORK_NAME} \
    --volume ${DBSERVER_VOLUME}:/data/db \
    --env MONGO_INITDB_ROOT_USERNAME=${DBSERVER_USER} \
    --env MONGO_INITDB_ROOT_PASSWORD=${DBSERVER_PASS} \
    ${DBSERVER_BASE}

# INITIALIZE DBServer
# Database initial data
cat << 'EOF' > data.json
{"_id":{"$oid":"5fa2ca6abe7a379ec4234883"},"Name":"Contenedores"}
{"_id":{"$oid":"5fa2ca6abe7a379ec4234884"},"Name":"Kubernetes"}
{"_id":{"$oid":"5fa2ca6abe7a379ec4234885"},"Name":"Ansible"}
{"_id":{"$oid":"5fa2ca6abe7a379ec4234886"},"Name":"Terraform"}
EOF

# SH file for data import
rm -f initialize_db.sh
cat << EOF > initialize_db.sh
#!/bin/bash
mongoimport -u ${DBSERVER_USER} -p ${DBSERVER_PASS} \
    --authenticationDatabase admin \
    -d TopicstoreDb -c Topics \
    data.json
rm -f data.json initialize_db.sh
EOF

# Copy files to root dir of mongo server
docker cp data.json ${DBSERVER_NAME}:/
docker cp initialize_db.sh ${DBSERVER_NAME}:/

# Clean files from local
rm -f initialize_db.sh data.json

# wait 5 secs until
sleep 5
# Initialize DB and import data in contaner
docker exec -t ${DBSERVER_NAME} /bin/bash /initialize_db.sh

# TEST DBSERVER
# Launch Mongo express for testing DBServer
set -x
docker run --detach --rm \
    --name ${DBSERVER_TEST_NAME} \
    --network ${NERTWORK_NAME} \
    --env ME_CONFIG_MONGODB_ADMINUSERNAME=${DBSERVER_USER} \
    --env ME_CONFIG_MONGODB_ADMINPASSWORD=${DBSERVER_PASS} \
    --env ME_CONFIG_MONGODB_URL=${DBSERVER_URL} \
    --publish=${DBSERVER_TEST_PORT}:${DBSERVER_TEST_PORT} \
    ${DBSERVER_TEST_BASE}
set +x

# test
for reg in Contenedores Kubernetes Ansible Terraform
do
    simpleTest "${reg} in Topics" \
        "Check if exists a registry with name ${reg} in library Topics" \
        "http://localhost:${DBSERVER_TEST_PORT}/db/TopicstoreDb/Topics" "${reg}" 5
    
    TMP=$?
    [[ ${TMP} -ne 0 ]] \
        && docker stop ${DBSERVER_TEST_NAME} > /dev/null \
        && exit ${TMP}
done

docker stop ${DBSERVER_TEST_NAME}


# BACKEND ==========================================================================================
# Stop container if it is running
[ $(docker ps -a | grep -c ${BACKEND_NAME}) -eq 1 ] && \
    docker stop ${BACKEND_NAME}

# Remove image if exists
[ $(docker images --format "{{.Repository}}:{{.Tag}}" | grep -c ${BACKEND_NAME}:latest) -ne 0 ] \
    && docker rmi -f ${BACKEND_NAME}

# Clone folder
echo -e "\e[1mGit clone\e[0m ${REPO_URL}/tree/master/${BACKEND_REPO}"
cloneGitSubfolder "${REPO_URL}" "${BACKEND_REPO}" "${PWD}/tmp"

# Patches
cat << EOF > appsettings.json.patch
--- tmp/appsettings.json $(stat -c %z tmp/appsettings.json)
+++ tmp/appsettings.json $(date "+%Y-%m-%d %H:%M:%S.%s %z")
@@ -1,6 +1,6 @@
 {
   "TopicstoreDatabaseSettings": {
-    "ConnectionString": "mongodb://localhost:27017",
+    "ConnectionString": "mongodb://${DBSERVER_USER}:${DBSERVER_PASS}@${DBSERVER_NAME}:${DBSERVER_PORT}",
     "TopicsCollectionName": "Topics",
     "DatabaseName": "TopicstoreDb"
   },
EOF

patch tmp/appsettings.json < appsettings.json.patch
rm appsettings.json.patch

# Create Image
cat <<EOF > Dockerfile
FROM ${BACKEND_BASE} AS base
WORKDIR /app 
ENV ASPNETCORE_URLS=http://+:5000
EXPOSE ${BACKEND_PORT}
RUN adduser -u 5678 --disabled-password --gecos "" appuser && chown -R appuser /app
USER appuser

FROM ${BACKEND_BUILD} AS build
WORKDIR /src
COPY tmp/backend.csproj .
RUN dotnet restore
COPY tmp ./
RUN dotnet build "backend.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "backend.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "backend.dll"]
EOF

cat Dockerfile
docker build . --tag ${BACKEND_NAME}:latest --compress
rm -Rf tmp Dockerfile

# Launch container
set -x
docker run --detach --rm \
    --name ${BACKEND_NAME} \
    --network ${NERTWORK_NAME} \
    ${BACKEND_NAME}:latest
set +x

# TEST BACKEND
# Launch alpine for testing backend
set -x
docker run --detach --rm -t\
    --name ${BACKEND_TEST_NAME} \
    --network ${NERTWORK_NAME} \
    ${BACKEND_TEST_BASE} sleep 3600
set +x

# test
CMD="docker exec -t ${BACKEND_TEST_NAME} wget -qO- "
CMD="${CMD} http://${BACKEND_NAME}:${BACKEND_PORT}/api/topics"
for reg in Contenedores Kubernetes Ansible Terraform
do
    simpleTest "${reg} in Topics throw Topics API" \
        "Check if exists a registry with name ${reg} in library Topics" \
        "" "${reg}" 5 "${CMD}"
    TMP=$?
    [[ ${TMP} -ne 0 ]] \
        && docker stop ${BACKEND_TEST_NAME} > /dev/null \
        && exit ${TMP}
done
unset CMD

docker stop -t 0 ${BACKEND_TEST_NAME}


# FRONTEND =========================================================================================
# Stop container if it is running
set -x
[ $(docker ps -a | grep -c ${FRONTEND_NAME}) -eq 1 ] && \
    docker stop ${FRONTEND_NAME}

# Remove image if exists
[ $(docker images --format "{{.Repository}}:{{.Tag}}" | grep -c ${FRONTEND_NAME}:latest) -ne 0 ] \
    && docker rmi -f ${FRONTEND_NAME}
set +x
# Clone folder
echo -e "\e[1mGit clone\e[0m ${REPO_URL}/tree/master/${FRONTEND_REPO}"
cloneGitSubfolder "${REPO_URL}" "${FRONTEND_REPO}" "${PWD}/tmp"

# Create Image
cat << EOF > Dockerfile
FROM ${FRONTEND_BASE}
LABEL mantainer="Imanol.Valero@live.com"
LABEL project="lemoncode"
WORKDIR /app
COPY tmp .
RUN npm install
EXPOSE ${FRONTEND_PORT}
CMD ["npm", "start"]
EOF
cat Dockerfile
docker build . --tag ${FRONTEND_NAME}:latest --compress
rm -Rf tmp Dockerfile

# Launch container
set -x
docker run --detach --rm \
    --name ${FRONTEND_NAME} \
    --network ${NERTWORK_NAME} \
    --env API_URI=http://${BACKEND_NAME}:${BACKEND_PORT}/api/topics \
    --publish ${PORT}:${FRONTEND_PORT} \
    ${FRONTEND_NAME}:latest
set +x

# TEST Front End
# test
for reg in Contenedores Kubernetes Ansible Terraform
do
    simpleTest "${reg} in Topics throw frontend" \
        "Check if exists a registry with name ${reg} in library Topics" \
        "http://localhost:${PORT}" "${reg}" 5
    
    TMP=$?
    [[ ${TMP} -ne 0 ]] && exit ${TMP}
done

echo "It''s done! you can test by yourself on http://localhost:${PORT}"

#===================================================================================================
read -p 'Do you want to clean everything before exit [Y|n]? ' TMP
TMP=${TMP::1}
[[ "${TMP,,}" == "y" ]] && \
    docker stop ${FRONTEND_NAME} ${BACKEND_NAME} ${DBSERVER_NAME} && \
    docker rmi ${FRONTEND_NAME}:latest ${BACKEND_NAME}:latest  && \
    docker volume rm ${DBSERVER_VOLUME} && \
    docker network rm ${NERTWORK_NAME} && \
    exit

#===================================================================================================
read -p 'Do you want to launch ejercicio2.yaml with docker-compose [y|N]? ' TMP
TMP=${TMP::1}
[[ "${TMP,,}" != "y" ]] && exit

echo "Stopping this script launched containers"
docker stop ${FRONTEND_NAME} ${BACKEND_NAME} ${DBSERVER_NAME}

echo "Launching Ejercicio2.ymal with docker-compose"
docker-compose --file ejercicio2.yaml up --detach

echo "It''s done! you can test by yourself on http://localhost:${PORT}"

#===================================================================================================
read -p 'Do you want to clean everything before exit [Y|n]? ' TMP
TMP=${TMP::1}
[[ "${TMP,,}" == "y" ]] && \
    docker-compose --file ejercicio2.yaml down && \
    docker rmi ${FRONTEND_NAME}:latest ${BACKEND_NAME}:latest  && \
    docker volume rm ${DBSERVER_VOLUME} && \
    docker network rm ${NERTWORK_NAME}