#!/bin/bash

SRC_REPO=https://github.com/Lemoncode/bootcamp-devops-lemoncode
SRC_DIR=01-contenedores/contenedores-i/lemoncoders-web
NGINX_NAME=lemoncoders-web
NGINX_NET=${NGINX_NAME}-net
NGINX_PORT=80
LOCAL_PORT=9999
CYPRESS_NAME=my-cypress

# Clone website
mkdir temp web
cd temp
git init
git remote add -f origin ${SRC_REPO}
git config core.sparseCheckout true
echo "${SRC_DIR}" >> .git/info/sparse-checkout
git pull origin master
cd ..
mv temp/${SRC_DIR}/* web/
rm -Rf temp
ls web

# Create network
[ $(docker network ls | grep -c ${NGINX_NET}) -eq 0 ] && \
    docker network create ${NGINX_NET}

# Launch website
docker run --detach --rm \
    --name ${NGINX_NAME} \
    --network ${NGINX_NET} \
    -v ${PWD}/web:/usr/share/nginx/html:ro  \
    --publish ${LOCAL_PORT}:${NGINX_PORT} \
    nginx:alpine


sleep 5
# Cypres end-to-end test spec
mkdir -p e2e/cypress/integration
cat << EOF > e2e/cypress/integration/spec.js
describe('Check if website is working correctly', () => {
    it('Check main titles', () => {
        cy.visit('http://${NGINX_NAME}:${NGINX_PORT}')
        cy.contains('Lemoncoders website')
        cy.contains('What is Lorem Ipsum?')
        cy.contains('Why do we use it?')
    })
})
EOF
# Cypres config file
cat << EOF > e2e/cypress.json
{
    "baseUrl": null,
    "watchForFileChanges":false
}
EOF

# Fix missing dependency. 
# https://github.com/cypress-io/cypress-docker-images/issues/304
CMD="apt-get update &&"
CMD="${CMD} apt-get install -y --no-install-recommends libgbm-dev &&"
CMD="${CMD} npx cypress run --headless --browser chrome"
CMD=(bash -c "${CMD}")

# Launch tests
docker run -t --rm \
    --name ${CYPRESS_NAME} \
    --network ${NGINX_NET} \
    -v ${PWD}/e2e:/app \
    -w /app -e CYPRESS_VIDEO=false \
    cypress/browsers:chrome69 "${CMD[@]}"

# Clean temp volume
sudo rm -Rf e2e

# Remove containers
docker stop ${NGINX_NAME}

# Clean website
rm -Rf web

# Remove network
docker network rm ${NGINX_NET}