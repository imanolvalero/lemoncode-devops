# Modulo 2 - Contenedores Docker - Deberes
## Sesion 1 - Introducción
### Ejercicio 1 - Crear contenedores de `MongoDB` y `MongoDB Express`
[[Ver a la solucion]](./modulo2-docker/deberes/sesion1/ejercicio1.sh)

Crea un contenedor con `MongoDB`, protegido por usuario y contraseña.
Conectate utilizando `Mongo Compass`.
Crea una base de datos llamada `Library` con una colección llamada `Books`.

Importa los datos de [books.json](https://raw.githubusercontent.com/Lemoncode/bootcamp-devops-lemoncode/master/01-contenedores/contenedores-i/books.json) en el directorio de la unidad.

![sesion 1, ejercicio 1](../../assets/ses1-ejer1.png)

__Nota:__ Se ha reemplazado `Mongo Compass`, que es una aplicación de escritorio, por `Mongo Express`, que es una aplicacion web. La razón es que es más sencillo de testear y se consigue el mismo objetivo.

### Ejercicio 2 - Crear contenedor `Nginx`
[[Ver a la solucion]](./modulo2-docker/deberes/sesion1/ejercicio2.sh)

Crea un contenedor llamado `lemoncoders-web`, con `Nginx`, accesible desde `http://localhost:9999 `

Copia el contenido de la carpeta [lemoncoders-web](https://github.com/Lemoncode/bootcamp-devops-lemoncode/tree/master/01-contenedores/contenedores-i/lemoncoders-web) de la unidad en la ruta que sirve este servidor.

Ejecuta `ls` desde fuera para ver que el contenido se ha copiado correctamente.

Accede a través del navegador de tu máquina.

![sesion 1, ejercicio 2](../../assets/ses1-ejer2.png)

### Ejercicio 3 - Limpiar sistema
[[Ver a la solucion]](./modulo2-docker/deberes/sesion1/ejercicio3.sh)

Eliminar todos los contenedores que tienes ejecutándose en tu máquina.
![sesion 1, ejercicio 3 CLI](../../assets/ses1-ejer3-cli.png)
![sesion 1, ejercicio 3 desktop](../../assets/ses1-ejer3-desktop.png)

