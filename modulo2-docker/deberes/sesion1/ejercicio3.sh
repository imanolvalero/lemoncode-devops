#!/bin/bash

docker container prune -f
docker network prune -f
docker volume prune -f
docker images -a --digests | grep sha | awk '{ print $4 }' | xargs docker rmi
