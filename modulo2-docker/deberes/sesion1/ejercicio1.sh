#!/bin/bash

BOOKS_URL=https://raw.githubusercontent.com/Lemoncode/bootcamp-devops-lemoncode
BOOKS_URL=${BOOKS_URL}/master/01-contenedores/contenedores-i/books.json
USER=root
PASS=pass
MONGO_NET=my-mongo-net
MONGO_NAME=my-mongo
MONGO_PORT=27017
MONGO_URL=mongodb://${USER}:${PASS}@${MONGO_NAME}:${MONGO_PORT}
EXPRESS_NAME=my-mongo-express
EXPRESS_PORT=8081
LOCAL_PORT=8081
CYPRESS_NAME=my-cypress

# db init script
rm -f initialize_db.js
cat << 'EOF' > initialize_db.js
db.createCollection("Books");
exit
EOF

# SH file for data import
rm -f initialize_db.sh
cat << EOF > initialize_db.sh
#!/bin/bash
mongosh  -u ${USER} -p ${PASS} --authenticationDatabase admin ${MONGO_URL} initialize_db.js
mongoimport -u ${USER} -p ${PASS} --authenticationDatabase admin -d Library -c Books books.json
rm -f initialize_db.js books.json initialize_db.sh
EOF

# JSON file with data to import
rm -f books.json
wget --quiet --output-document=books.json ${BOOKS_URL}

# Create network
[ $(docker network ls | grep -c ${MONGO_NET}) -eq 0 ] && \
    docker network create ${MONGO_NET}

# Create mongo container
docker run --detach --rm \
    --name ${MONGO_NAME} \
    --network ${MONGO_NET} \
    --env MONGO_INITDB_ROOT_USERNAME=${USER} \
    --env MONGO_INITDB_ROOT_PASSWORD=${PASS} \
    mongo:latest

# Copy files to root dir of mongo server
docker cp initialize_db.js my-mongo:/
docker cp initialize_db.sh my-mongo:/
docker cp books.json my-mongo:/

# Clean files from local
rm -f initialize_db.js initialize_db.sh books.json

sleep 5
# Initialize DB and import data in contaner
docker exec -t my-mongo /bin/bash /initialize_db.sh

sleep 5
# Create mongo-express container for testing
docker run --detach --rm \
    --name ${EXPRESS_NAME} \
    --network ${MONGO_NET} \
    --env ME_CONFIG_MONGODB_ADMINUSERNAME=${USER} \
    --env ME_CONFIG_MONGODB_ADMINPASSWORD=${PASS} \
    --env ME_CONFIG_MONGODB_URL=${MONGO_URL} \
    --publish=${LOCAL_PORT}:${EXPRESS_PORT} \
    mongo-express:latest

sleep 5
# Cypres end-to-end test spec
mkdir -p e2e/cypress/integration
cat << EOF > e2e/cypress/integration/spec.js
describe('Check if everything is working', () => {
    it('Check imported records throw mongo-express', () => {
        cy.visit('http://${EXPRESS_NAME}:${EXPRESS_PORT}/db/Library/Books')
        cy.contains('Docker in Action, Second Edition')
        cy.contains('Jeff Nickoloff and Stephen Kuenzli')
        cy.contains('Kubernetes in Action, Second Edition')
        cy.contains('Marko Lukša')
    })
})
EOF
# Cypres config file
cat << EOF > e2e/cypress.json
{
    "baseUrl": null,
    "watchForFileChanges":false
}
EOF

# Fix missing dependency. 
# https://github.com/cypress-io/cypress-docker-images/issues/304
CMD="apt-get update "
CMD="${CMD} && apt-get install -y --no-install-recommends libgbm-dev "
CMD="${CMD} && npx cypress run --headless --browser chrome"
CMD=(bash -c "${CMD}")

# Launch tests
docker run -t --rm \
    --name ${CYPRESS_NAME} \
    --network ${MONGO_NET} \
    -v ${PWD}/e2e:/app \
    -w /app -e CYPRESS_VIDEO=false \
    cypress/browsers:chrome69 "${CMD[@]}"

# Clean temp volume
sudo rm -Rf e2e

# Remove containers
docker stop ${EXPRESS_NAME}
docker stop ${MONGO_NAME}

# Remove network
docker network rm ${MONGO_NET}