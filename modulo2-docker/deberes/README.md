# Modulo 2 - Contenedores Docker - Deberes

### Sesion 1 - Presentacion
[[Ir al enunciado]](./modulo2-docker/deberes/sesion1)
* Ejercicio 1 - Crear contenedores de `MongoDB` y `MongoDB Express` [[Ir a la solucion]](./modulo2-docker/deberes/sesion1/ejercicio1.sh)
* Ejercicio 2 - Crear contenedor `Nginx` [[Ir a la solucion]](./modulo2-docker/deberes/sesion1/ejercicio2.sh)
* Ejercicio 3 - Limpiar sistema [[Ir a la solucion]](./modulo2-docker/deberes/sesion1/ejercicio3.sh)


### Sesion 2 - Imagenes 
[[Ir al enunciado]](./modulo2-docker/deberes/sesion2)
* Ejercicio 1 - Crear imagen de `Apache` [[Ir a la solucion]](./modulo2-docker/deberes/sesion2/ejercicio1.sh)
* Ejercicio 2 - Crear contenedor desde la nueva imagen [[Ir a la solucion]](./modulo2-docker/deberes/sesion2/ejercicio2.sh)
* Ejercicio 3 - Inspeccionar la imagen [[Ir a la solucion]](./modulo2-docker/deberes/sesion2/ejercicio3.sh)


### Sesion 3 - Contenerización
[[Ir al enunciado]](./modulo2-docker/deberes/sesion3)
* Ejercicio 1 - Containerizar aplicacion `Node.js` [[Ir a la solucion]](./modulo2-docker/deberes/sesion3/ejercicio1.sh)

### Sesion 4 - Volumenes
[[Ir al enunciado]](./modulo2-docker/deberes/sesion4)
* Ejercicio 1 - Lanzar contenedor con volumen tipo `volume` [[Ir a la solucion]](./modulo2-docker/deberes/sesion4/ejercicio1.sh)
* Ejercicio 2 - El contenedor muere, el volumen vive [[Ir a la solucion]](./modulo2-docker/deberes/sesion4/ejercicio2.sh)
* Ejercicio 3 - Lanzar contenedor con volumen tipo `bind` [[Ir a la solucion]](./modulo2-docker/deberes/sesion4/ejercicio3.sh)

### Sesion 5 - Redes
[[Ir al enunciado]](./modulo2-docker/deberes/sesion5)
* Ejercicio 1 - Redes [[Ir a la solucion]](./modulo2-docker/deberes/sesion5/ejercicio1.sh)

### Sesion 6 - Docker compose
[[Ir al enunciado]](./modulo2-docker/deberes/sesion6)
* Ejercicio 1 [[Ir a la solucion]](./modulo2-docker/deberes/sesion6/ejercicio1.sh)
