#!/bin/bash

REPO_URL=https://github.com/Lemoncode/bootcamp-devops-lemoncode
REPO_DIR=01-contenedores/contenedores-iii/hello-lemoncoder

APP_DIR=$(mktemp -d -t deberes.XXXXX)
TEST_DIR=$(mktemp -d -t deberes.XXXXX)
TEST_PORT=8080

APP_BASE=node:16.11.0-alpine3.12
APP_NAME=hello-lemoncoder
APP_TAG=new
APP=${APP_NAME}:${APP_TAG}
APP_PORT=4000


function cloneGitSubfolder(){
    local repo="$1"
    local rdir="$2"
    local dest="$3"
    local cdir="${PWD}"
    local temp=$(mktemp -d -t cloneGitSubFolder.XXXXX)
    
    cd ${temp}
    git init &> /dev/null
    git remote add -f origin ${repo} &> /dev/null
    git config core.sparseCheckout true &> /dev/null
    echo "${rdir}" >> .git/info/sparse-checkout
    git pull origin master &> /dev/null
    cd ${cdir}
    rsync -r -f "-x .git" ${temp}/${rdir}/* ${dest}
    rm -Rf ${temp}
    return 0
}

function simpleTest(){
    local name="${1:-"Simple test"}"
    local desc="${2:-""}"
    local url=${3:-http://localhost}
    local text="${4:-""}"
    local delay="${5:-0}"

    echo -e "\e[1mSimple Test: ${name}\e[0m\n$desc"
    [[ ${delay} -gt 0 ]] && (
        local bar=""
        local count=0
        while [ ${count} -lt ${delay} ]; do
            bar=""            
            local i=0
            while [ ${i} -lt ${delay} ]; do
                [[ ${i} -gt count ]] && bar="${bar} " || bar="${bar}>"
                (( i++ ))
            done
            echo -ne "\e[33mWaiting [${bar}] \e[0m\r"
            (( count++ ))
            sleep 1
        done
    )
    echo -ne "\e[33mTesting...\e[0m                                          \r"
    [[ $(curl -s ${url} | grep -c "${text}") -eq 1 ]] \
        && (echo -e "\e[32mTest passed\e[0m\n" && return 0) \
        || (echo -e "\e[31mTest failed\e[0m\n" && return 1)
}

# Clone folder
echo -e "\e[1mGit clone\e[0m ${REPO_URL}/tree/master/${REPO_DIR}"
cloneGitSubfolder "${REPO_URL}" "${REPO_DIR}" "${APP_DIR}"

# Test app on local
echo -e "\e[1mTest App local in local env\e[0m"
TMP=${PWD}
cp -r ${APP_DIR}/* ${TEST_DIR}
cd ${TEST_DIR}
npm install > /dev/null
npm start > /dev/null &
NPM_PID=$!
cd ${TMP}

simpleTest "Application is running in local" \
    "Asserts appliation is running directly from NodeJs" \
    http://localhost:${APP_PORT} \
    "Congratulations!" 5

TMP=$?
kill ${NPM_PID}
rm -Rf ${TEST_DIR}
[[ ${TMP} -ne 0 ]] && exit ${TMP}


# Create image
echo -e "\e[1mCreate Docker image\e[0m"
# Remove image if exists
TMP=$(docker images --format "{{.Repository}}:{{.Tag}}" | grep -c ${APP})
[[ ${TMP} -ne 0 ]] \
    && docker rmi -f ${APP} > /dev/null

# Create Dockerfile
cat << EOF > Dockerfile
FROM ${APP_BASE}
LABEL mantainer="Imanol.Valero@live.com"
LABEL project="lemoncode"
WORKDIR /app
ADD ./app.tar .
RUN npm install
EXPOSE ${APP_PORT}
CMD ["npm", "start"]
EOF

# Prepare files
tar cvf app.tar -C ${APP_DIR} . > /dev/null

# Create new image
docker build . --tag ${APP} --compress > /dev/null
# Clean 
rm Dockerfile app.tar

# Launch and test new image
echo -e "\e[1mLaunch and test new image\e[0m"
TMP=$(date "+%Y%m%d%H%M%S")
docker run --rm -d \
    --name test-${TMP} \
    --publish ${TEST_PORT}:${APP_PORT} \
    ${APP} > /dev/null

simpleTest "Test ${APP_NAME} image" \
    "Asserts ${APP_NAME} is running correctly" \
    http://localhost:${TEST_PORT} \
    "Congratulations!" 5

docker stop test-${TMP} > /dev/null
echo -e "\e[32mThe application has been correctly containerized\e[0m"