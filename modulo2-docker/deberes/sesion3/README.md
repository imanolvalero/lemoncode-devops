# Modulo 2 - Contenedores Docker - Deberes
## Sesion 3 - Contenerización
### Ejercicio 1 - Containerizar aplicacion `Node.js`
[[Ir a la solucion]](./modulo2-docker/deberes/sesion3/ejercicio1.sh)

Dockeriza la carpeta [hello-lemoncoder](https://github.com/Lemoncode/bootcamp-devops-lemoncode/tree/master/01-contenedores/contenedores-iii/hello-lemoncoder)
Antes de hacerlo __¡pruébala en local!__
Genera la imagen con el __VS Code__
Ejecuta un contenedor con la imagen

__NOTA:__ Con el fin de realizar un entregable sencillo, sin entrar en automatizacion de GUI, todas las operaciones se realizarán usando la __shell__, en vez de __VS Code__

Resultado:
![sesion 3, ejercicio 1](../../assets/ses3-ejer1.png)

