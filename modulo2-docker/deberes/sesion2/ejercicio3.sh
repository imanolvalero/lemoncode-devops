#!/bin/bash -x

IMAGE=simple-httpd:new
FILTER='first | .RootFS | .Layers | length'

echo $(docker inspect ${IMAGE} | jq -r "${FILTER[@]}")
