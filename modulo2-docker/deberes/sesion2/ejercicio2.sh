#!/bin/bash

IMG_NAME=my_apache
IMG_PORT=80
LOCAL_PORT=5050
CYPRESS_NAME=my-cypress

# Launch website
docker run --detach --rm \
    --name ${IMG_NAME} \
    -v ${PWD}/web:/usr/share/nginx/html:ro  \
    --publish ${LOCAL_PORT}:${IMG_PORT} \
    simple-httpd:new


sleep 5
# Test application is accesible from host
# Test image creation
echo -e "\e[1mTest\e[0m: Application ${IMG_NAME} is accesible from  host"
[ $(curl -s http://localhost:5050 | grep -c "I'm inside of a container. Help\!") -eq 1 ] \
    && echo -e "\e[32mTest passed\e[0m" \
    || (echo -e "\e[31mTest failed\e[0m" && exit 1)

# Remove containers
docker stop ${IMG_NAME}

# Clean website
rm -Rf web

