#!/bin/bash -x

SRC_REPO=https://github.com/Lemoncode/bootcamp-devops-lemoncode
SRC_DIR=01-contenedores/contenedores-ii/content
IMG_NAME=simple-httpd
IMG_TAG=new
IMG=${IMG_NAME}:${IMG_TAG}
IMG_PORT=80
DOCKERHUB_USER=imanolvalero
DOCKERHUB_URL=https://hub.docker.com
TIMESTAMP=$(date '+%Y%m%d%H%M%S')
CYPRESS_NAME=my-cypress


# Clone website
mkdir temp content
cd temp
git init
git remote add -f origin ${SRC_REPO}
git config core.sparseCheckout true
echo "${SRC_DIR}" >> .git/info/sparse-checkout
git pull origin master
cd ..
mv temp/${SRC_DIR}/* content/
rm -Rf temp
ls content

# Remove image if exists
[ $(docker images --format "{{.Repository}}:{{.Tag}}" | grep -c ${IMG}) -ne 0 ] \
    && docker rmi -f ${IMG}

# Create Dockerfile
cat <<'EOF' > Dockerfile
FROM httpd:alpine
LABEL mantainer="Imanol.Valero@live.com"
LABEL project="lemoncode"
EXPOSE 80
COPY content/ /usr/local/apache2/htdocs/
EOF

# Create new image
docker build . --tag ${IMG} --compress

# Clean 
rm -Rf content Dockerfile

# Test image creation
echo -e "\e[1mTest\e[0m: Image ${IMG} has been created"
[ $(docker images --format "{{.Repository}}:{{.Tag}}" | grep -c ${IMG}) -eq 1 ] \
    && echo -e "\e[32mTest passed\e[0m" \
    || (echo -e "\e[31mTest failed\e[0m" && exit 1)


# Publish image on Docker Hub
function yes_or_no {
    while true; do
        read -p "$* [y/n]: " yn
        case $yn in
            [Yy]*) return 0 ;;  
            [Nn]*) return 1 ;;
        esac
    done
}
yes_or_no 'Push image to DockerHub?'
[ $? -eq 1 ] && exit 0

# Login if user is not logged in yet
[ $(jq -r '.auths | length' ${HOME}/.docker/config.json) -eq 0 ] \
    && (
        docker login -u ${DOCKERHUB_USER}
        [ $? -ne 0 ] && echo 'Login error' && exit 0
    )

# Push the image
docker tag  ${IMG} ${DOCKERHUB_USER}/${IMG_NAME}:${TIMESTAMP}
docker push ${DOCKERHUB_USER}/${IMG_NAME}:${TIMESTAMP}
