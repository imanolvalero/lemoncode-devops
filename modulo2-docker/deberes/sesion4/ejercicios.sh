#!/bin/bash
IFS=$'\n'

REPO_URL=https://github.com/Lemoncode/bootcamp-devops-lemoncode
REPO_DIR=01-contenedores/contenedores-iv/gallery-app

APP_WORKDIR=/usr/src/app
APP_DIR=$(basename ${APP_WORKDIR})
APP_PICTURE_DIR=images
APP_NAME=galleryapp
APP_TAG=prod
APP=${APP_NAME}:${APP_TAG}
APP_PORT=8080

VOLUME_NAME=images
VOLUME_PATH=""

TEST_PORT=8080
TIMESTAMP=$(date "+%Y%m%d%H%M%S")
PICTURE_DIR=$(readlink -f ~/Imágenes)



function cloneGitSubfolder(){
    local repo="$1"
    local rdir="$2"
    local dest="$3"
    local cdir="${PWD}"
    local temp=$(mktemp -d -t cloneGitSubFolder.XXXXX)
    
    cd ${temp}
    git init &> /dev/null
    git remote add -f origin ${repo} &> /dev/null
    git config core.sparseCheckout true &> /dev/null
    echo "${rdir}" >> .git/info/sparse-checkout
    git pull origin master &> /dev/null
    cd ${cdir}
    rsync -r -f "-x .git" ${temp}/${rdir}/* ${dest}
    rm -Rf ${temp}
    return 0
}


function simpleTest(){
    local name="${1:-"Simple test"}"
    local desc="${2:-""}"
    local url=${3:-http://localhost}
    local text="${4:-""}"
    local delay="${5:-0}"

    echo -e "\e[1mSimple Test: ${name}\e[0m\n$desc"
    [[ ${delay} -gt 0 ]] && (
        local bar=""
        local count=0
        while [ ${count} -lt ${delay} ]; do
            bar=""            
            local i=0
            while [ ${i} -lt ${delay} ]; do
                [[ ${i} -gt count ]] && bar="${bar} " || bar="${bar}>"
                (( i++ ))
            done
            echo -ne "\e[33mWaiting [${bar}] \e[0m\r"
            (( count++ ))
            sleep 1
        done
    )
    echo -ne "\e[33mTesting...\e[0m                                          \r"
    [[ $(curl -s ${url} | grep -c "${text}") -eq 1 ]] \
        && (echo -e "\e[32mTest passed\e[0m\n" && return 0) \
        || (echo -e "\e[31mTest failed\e[0m\n" && return 1)
}


# Clone folder
echo -e "\e[1mGit clone\e[0m ${REPO_URL}/tree/master/${REPO_DIR}"
cloneGitSubfolder "${REPO_URL}" "${REPO_DIR}" "${APP_DIR}"

# Create image
echo -e "\e[1mCreate Docker image\e[0m"
# Remove image if exists
TMP=$(docker images --format "{{.Repository}}:{{.Tag}}" | grep -c ${APP})
[[ ${TMP} -ne 0 ]] \
    && docker rmi -f ${APP} > /dev/null

# Create new image and clean
TMP=${PWD}
cd ${APP_DIR}
docker build . --tag ${APP} --compress > /dev/null
cd ${TMP}
rm -Rf ${APP_DIR}


# Exercise 1: Create Volume, attach it to container, copy files and test
echo -e "\e[33mEjercicio 1\e[0m"
echo -e "\e[1mCrear Volumen\e[0m"
[[ -e $(docker volume ls | grep ${VOLUME_NAME}) ]] \
    && docker volume rm ${VOLUME_NAME}
docker volume create ${VOLUME_NAME}
# Launch and test new image
echo -e "\e[1mLanzar y testear la nueva imagen\e[0m"
docker run --rm -d \
    --name test-${TIMESTAMP} \
    --publish ${TEST_PORT}:${APP_PORT} \
    --volume ${VOLUME_NAME}:${APP_WORKDIR}/${APP_PICTURE_DIR}:ro \
    ${APP} > /dev/null
simpleTest "Comprobar que el contenedor se ha levantado correctamente" \
    "Asegurar que ${APP_NAME} está ejecutandose" \
    http://localhost:${TEST_PORT} \
    "Image Gallery" 5
TMP=$?
[[ ${TMP} -ne 0 ]] \
    && docker stop test-${TIMESTAMP} > /dev/null \
    && exit ${TMP}

# Check there is no image
echo -e "\e[1mComprobar que NO hay imagenes (PNG)\e[0m"
simpleTest "Comprobar que la aplicación no muestra imagenes" \
    "Asegura que ${APP_NAME} no muestra imagenes (PNG). \e[33mDEBE FALLAR\e[0m" \
    http://localhost:${TEST_PORT} \
    ".png"
TMP=$?
[[ ${TMP} -ne 1 ]] \
    && docker stop test-${TIMESTAMP} > /dev/null \
    && exit ${TMP}

# Copy some pictures 
echo -e "\e[1mCopiar varias imagenes de prueba (PNG). \e[33mNecesita poderes de Super Vaca.\e[0m"
VOLUME_PATH=$(docker volume inspect ${VOLUME_NAME} | jq -r '.[].Mountpoint')
find ${PICTURE_DIR} -maxdepth 1 -type f -iname "*.png"  \
    | head -5 \
    | sudo xargs cp -t ${VOLUME_PATH}

# Test copied images are being showed in App
echo -e "\e[1mComprobar que la aplicacion muestra las imagenes copiadas\e[0m"
set -x
PICS=$(ls -p ${VOLUME_PATH} | grep -v /)
set +x
for PIC in ${PICS}; do
    simpleTest "Comprobar que las imágenes coopiadas aparecen en la aplicacion" \
        "Asegura que ${APP_NAME} muestra la imagen ${PIC}" \
        http://localhost:${TEST_PORT} \
        "${PIC}"
    TMP=$?
    [[ ${TMP} -ne 0 ]] \
        && docker stop test-${TIMESTAMP} > /dev/null \
        && exit ${TMP}
done
echo -e "\e[32mEjercicio 1 completado con éxito\e[0m"


# Exercise 2: Remove container and check Volume content
echo -e "\e[33mEjercicio 2\e[0m"
echo -e "\e[1mParar el contenedor\e[0m"
docker stop test-${TIMESTAMP} > /dev/null 
echo -e "\e[1mListar el contenido del volumen\e[0m"
ls -lah ${VOLUME_PATH}
# Clean
docker volume rm ${VOLUME_NAME}
echo -e "\e[32mEjercicio 2 completado con éxito\e[0m"


# Exercise 3: Create a folder for a new container and check files
echo -e "\e[33mEjercicio 3\e[0m"
echo -e "\e[1mCrear una nueva carpeta con imagenes\e[0m"
TEST_DIR=$(mktemp -d -t deberes.XXXXX)
find ${PICTURE_DIR} -maxdepth 1 -type f -iname "*.png"  \
    | head -5 \
    | xargs cp -t ${TEST_DIR}

# Create a new container with the new folder mounted as volume
echo -e "\e[1mCrar un contenedor de ${IMG_NAME} con la nueva carpeta de imagenes\e[0m"
docker run --rm -d \
    --name test-${TIMESTAMP} \
    --publish ${TEST_PORT}:${APP_PORT} \
    --volume ${TEST_DIR}:${APP_WORKDIR}/${APP_PICTURE_DIR}:ro \
    ${APP} > /dev/null
simpleTest "Comprobar que el contenedor se ha levantado correctamente" \
    "Asegurar que ${APP_NAME} está ejecutandose" \
    http://localhost:${TEST_PORT} \
    "Image Gallery" 5
TMP=$?
[[ ${TMP} -ne 0 ]] \
    && docker stop test-${TIMESTAMP} > /dev/null \
    && exit ${TMP}

# Check the applciation shows all files of the new folder
echo -e "\e[1mComprobar que la aplicacion muestra todos los archvivos de la carpeta\e[0m"
PICS=$(ls -p ${TEST_DIR} | grep -v /)
for PIC in ${PICS}; do
    simpleTest "Comprobar que las imágenes coopiadas aparecen en la aplicacion" \
        "Asegura que ${APP_NAME} muestra la imagen ${PIC}" \
        http://localhost:${TEST_PORT} \
        "${PIC}"
    TMP=$?
    [[ ${TMP} -ne 0 ]] \
        && docker stop test-${TIMESTAMP} > /dev/null \
        && exit ${TMP}
done

# Make changes on folder and assert the applications reflets them
echo -e "\e[1mEliminar un archivo de la carpeta de imagenes\e[0m"
PIC=$(ls -p ${TEST_DIR} | grep -v / | head -1)
rm ${TEST_DIR}/${PIC}
simpleTest "Comprobar que al aplicacion refleja los cambios" \
    "Comprobar que ${APP_NAME} NO muestra la imagen ${PIC}. \e[33mDEBE FALLAR\e[0m" \
    http://localhost:${TEST_PORT} \
    "${PIC}"
TMP=$?
[[ ${TMP} -ne 1 ]] \
    && docker stop test-${TIMESTAMP} > /dev/null \
    && exit ${TMP}

docker stop test-${TIMESTAMP} > /dev/null
echo -e "\e[32mEjercicio 3 completado con éxito\e[0m"
