# Modulo 2 - Contenedores Docker - Deberes
## Sesion 4 - Volumenes
__NOTA:__ Al estar relaccionados, todos los ejercicios van en el mismo script
[[Ir a la solucion]](./modulo2-docker/deberes/sesion4/ejercicio1.sh)
### Ejercicio 1 - Lanzar contenedor con volumen tipo `volume`
Crea un contenedor que cree un __volumen__ llamado ```images``` y que utilice la imagen ```0gis0/galleryapp```
__NOTA:__ La imagen ```0gis0/galleryapp``` se generará a partir de este [Dockerfile](https://github.com/Lemoncode/bootcamp-devops-lemoncode/blob/master/01-contenedores/contenedores-iv/gallery-app/Dockerfile)

El volumen debe estar montado en la carpeta images del ```WORKDIR``` del contenedor 

Y copia en él algunas imágenes ¡tú eliges cómo!
__Resultado:__
![sesion 4, ejercicio 1](../../assets/ses4-ejer1.png)


### Ejercicio 2 - El contenedor muere, el volumen vive
[[Ir a la solucion]](./modulo2-docker/deberes/sesion4/ejercicio2.sh)

Elimina el contenedor del ejercicio anterior.

Comprueba que el volumen ```images``` sigue existiendo

Lista el contenido del mismo para ver que tus imágenes siguen ahí
__Resultado:__ 
![sesion 4, ejercicio 2](../../assets/ses4-ejer2.png)


### Ejercicio 3 - Lanzar contenedor con volumen tipo `bind`
[[Ir a la solucion]](./modulo2-docker/deberes/sesion4/ejercicio3.sh)
Crea un contenedor que utilice __bind mount__ sobre una carpeta que tengas con imágenes y que utilice la imagen ```0gis0/galleryapp```

El montaje debe estar montado en la carpeta images del ```WORKDIR``` del contenedor 

Altera el contenido de tu carpeta local y refresca la web ¿ves los cambios?
__Resultado:__
![sesion 4, ejercicio 3](../../assets/ses4-ejer3.png)
