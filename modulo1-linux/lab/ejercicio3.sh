#!/bin/bash

# set correct text
text=${1:-"Que me gusta la bash!!!!"}

# 1st excercise commands
mkdir -p foo/dummy foo/empty
echo $text > foo/dummy/file1.txt
touch foo/dummy/file2.txt

# 2nd excercise commands
cat foo/dummy/file1.txt > foo/dummy/file2.txt 
mv foo/dummy/file2.txt foo/empty/file2.txt 
