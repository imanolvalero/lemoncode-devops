#!/bin/bash

# constants
URL="https://es.wikipedia.org/wiki/DevOps"
TMP=$(mktemp)

# read word to find
word=$1
[[ -z "$word" ]] && echo "Modo de uso: $0 palabra-a-buscar" && exit

# download
wget $URL -O $TMP

# find line number
line=$(grep --line-number --ignore-case $word $TMP | awk -F":" '{ print $1 }'| head -1)

echo $line

#clean
rm --force $TMP
