# Modulo 1 - Linux - Laboratorio

## Ejercicio 1 - Manejo de archivos
[[Ir a la solución]](https://gitlab.com/imanolvalero/lemoncode-devops/-/blob/main/modulo1-linux/lab/ejercicio1.sh)

Crea mediante comandos de bash la siguiente jerarquía de ficheros y directorios.
```
foo/
├─ dummy/ 
│  ├─ file1.txt 
│  ├─ file2.txt
├─ empty/
```
Donde file1.txt debe contener el siguiente texto:
`Me encanta la bash!!`
Y file2.txt debe permanecer vacío.

## Ejercicio 2 - Manejo de contenido de archivos
[[Ir a la solución]](https://gitlab.com/imanolvalero/lemoncode-devops/-/blob/main/modulo1-linux/lab/ejercicio2.sh)

Mediante comandos de bash, vuelca el contenido de file1.txt a file2.txt y mueve file2.txt a la carpeta empty.
El resultado de los comandos ejecutados sobre la jerarquía anterior deben dar el siguiente resultado.
```
foo/
├─ dummy/
│  ├─ file1.txt
├─ empty/
  ├─ file2.txt
```
Donde file1.txt y file2.txt deben contener el siguiente texto:
`Me encanta la bash!!`

### Ejercicio 3 - Argumentos del script
[[Ir a la solución]](https://gitlab.com/imanolvalero/lemoncode-devops/-/blob/main/modulo1-linux/lab/ejercicio3.sh)

Crear un script de bash que agrupe los pasos de los ejercicios anteriores y además permita establecer el texto de file1.txt alimentándose como parámetro al invocarlo.
Si se le pasa un texto vacío al invocar el script, el texto de los ficheros, el texto por defecto será:
`Que me gusta la bash!!!!`

### Ejercicio 4 - Opcional
[[Ir a la solución]](https://gitlab.com/imanolvalero/lemoncode-devops/-/blob/main/modulo1-linux/lab/ejercicio4.sh)

Crea un script de bash que descargue el conetenido de una página web a un fichero.
Una vez descargado el fichero, que busque en el mismo una palabra dada (esta se pasará por parametro) y muestre por pantalla el número de linea donde aparece.
