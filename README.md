# Bootcamp Devops II - Ejercicios y laboratorios

Repositorio donde alojar todo lo practicado en la 2ª edición del Bootcamp DevOps de LemonCode


## Ejercicios
### Modulo 1 - Linux
* [Laboratorio](./modulo1-linux/lab)

### Modulo 2 - Docker
* [Sesion 1 - Introducción](./main/modulo2-docker/deberes/sesion1)
* [Sesion 2 - Imágenes](./main/modulo2-docker/deberes/sesion2)
* [Sesion 3 - Contenerización](.main/modulo2-docker/sesion3)
* [Sesion 4 - Volúmenes](./main/modulo2-docker/deberes/sesion4)
* [Sesion 5 - Redes](./modulo2-docker/deberes/sesion5)
* [Sesion 6 - Docker Swarm](./modulo2-docker/sesion6)
* [Laboratorio](./modulo2-docker/lab)

### Modulo 3 - Kubernetes
* [k8s-local-cluster](https://gitlab.com/imanolvalero/k8s-local-cluster) - Herramienta para levantar laboratorios de Kubernetes en Linux y libvirt usando Vagrant y Ansible
* [Laboratorio](./modulo3-kubernetes/lab)

### Modulo 4 - CI/CD
* [Laboratorio](./modulo4-cicd/lab)
