# Modulo 4 - CI/CD - Laboratorio
 
## Ejercios Jenkins
[[Ir a la carpeta]](./modulo4-cicd/lab/01-jenkins)

### Ejercicio 1. CI/CD de una Java + Gradle
 
Se trata de crear una pipeline con los stages `checkout`, `compile` y `unit test` de un proyecto proporcionado.
 
### Ejercicio 2. Modificar la pipeline para que utilice la imagen Docker de Gradle como build runner

Se trata de hacer lo mismo que en el ejercicio anterior, pero utilizando un contenedor de `docker` como `build runner`.



## Ejercicios GitLab
[[Ir a la carpeta]](./modulo4-cicd/lab/02-gitlab)

### Ejercicio 1. CI/CD de una aplicación spring

Se trata de crear una pipeline completa. Desde la compilación hasta su despliegue.
