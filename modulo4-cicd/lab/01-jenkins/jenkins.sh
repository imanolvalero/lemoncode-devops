#!/bin/bash

TASK_DIR=$1

[ -d "${TASK_DIR}" ] || \
( \
    echo -e "\n\e[1mThe directory \"${TASK_DIR}\" does not exists.\e[0m exiting...\n\n"  && \
    exit \
) 

REPO_URL=https://github.com/Lemoncode/bootcamp-devops-lemoncode
REPO_PATH=03-cd/exercises/jenkins-resources

SERVER_NAME=jenkins
SERVER_UI_PORT=8080
SERVER_AGENT_PORT=50000
SERVER_HOME=/var/jenkins_home

JAVA_OPTS="-Djenkins.install.runSetupWizard=false "
JAVA_OPTS+="-Dhudson.udp=-1 "
JAVA_OPTS+="-Djava.awt.headless=true "
JAVA_OPTS+="-Dorg.jenkinsci.plugins.durabletask.BourneShellScript.LAUNCH_DIAGNOSTICS=true "

JENKINS_OPTS="--argumentsRealm.roles.user=admin "
JENKINS_OPTS+="--argumentsRealm.passwd.admin=admin "
JENKINS_OPTS+="--argumentsRealm.roles.admin=admin "

JOB_NAME="lemoncode-challenge"

# AUX FUNCTIONS ====================================================================================
function waitUntilLog() {
    local msg="$1"
    local srv="$2"
    local log="$3"
    echo -e "\n\e[1mWaiting until ${msg}\e[0m"
    while [ $(docker logs ${srv} 2>&1 | grep -c "${log}") -eq 0 ]
    do
        sleep 1
    done
}

function waitUntilServerIsReady() {
    local srv="$1"
    waitUntilLog "server is ready" ${srv} "onReady: Jenkins is fully up and running"
}

function waitUntilServerIsUpdated() {
    local srv="$1"
    waitUntilLog "server checks updates" ${srv} "Finished Download metadata"
}

# CLEAN AND PREPARE DOCKER ENVIRONMENT =============================================================
# Remove containers if exists
[ $(docker ps -a | grep -c ${SERVER_NAME}-container) -ne 0 ] && \
    docker rm -f ${SERVER_NAME}-container

[ $(docker ps -a | grep -c ${SERVER_NAME}-docker) -ne 0 ] && \
    docker rm -f ${SERVER_NAME}-docker

# Prepare network
[ $(docker network ls | grep -c ${SERVER_NAME}-network) -eq 0 ] && \
    docker network create ${SERVER_NAME}-network

# Prepare Volumes
[ $(docker volume ls | grep -c ${SERVER_NAME}-certs) -gt 0 ] && \
    docker volume rm ${SERVER_NAME}-certs

[ $(docker volume ls | grep -c ${SERVER_NAME}-volume) -gt 0 ] && \
    docker volume rm ${SERVER_NAME}-volume
docker volume create ${SERVER_NAME}-volume

# BUILD IMAGE ======================================================================================
echo -e "\n\e[1mBuilding Jenkins server image\e[0m"
# Remove image if exists
[ $(docker images --format "{{.Repository}}:{{.Tag}}" | grep -c ${SERVER_NAME}-image:latest) -ne 0 ] \
    && docker rmi -f ${SERVER_NAME}-image:latest

# Build jenkins image
docker build ${TASK_DIR} --file ${TASK_DIR}/Dockerfile --tag ${SERVER_NAME}-image:latest --compress --force-rm

# LAUNCH SERVER ====================================================================================
# Launch container
echo -e "\n\e[1mPreparing Jenkins server.\e[0m - Launching server"
if [ -e "${TASK_DIR}/dind" ]
then
    docker volume create ${SERVER_NAME}-certs

    docker container run --rm --detach \
        --name ${SERVER_NAME}-docker \
        --network ${SERVER_NAME}-network \
        --network-alias docker \
        --volume ${SERVER_NAME}-certs:/certs/client \
        --volume ${SERVER_NAME}-volume:${SERVER_HOME} \
        --publish 2376:2376 \
        --env DOCKER_TLS_CERTDIR=/certs \
        --privileged \
        docker:dind

    docker run --detach \
        --name ${SERVER_NAME}-container \
        --network ${SERVER_NAME}-network \
        --volume ${SERVER_NAME}-certs:/certs/client \
        --volume ${SERVER_NAME}-volume:${SERVER_HOME} \
        --publish 8080:${SERVER_UI_PORT} \
        --publish 50000:${SERVER_AGENT_PORT} \
        --env "JAVA_OPTS=${JAVA_OPTS}" \
        --env "JENKINS_OPTS=${JENKINS_OPTS}" \
        --env DOCKER_HOST=tcp://docker:2376 \
        --env DOCKER_CERT_PATH=/certs/client \
        --env DOCKER_TLS_VERIFY=1 \
        --privileged \
        ${SERVER_NAME}-image:latest

else
    docker run --detach \
        --name ${SERVER_NAME}-container \
        --network ${SERVER_NAME}-network \
        --volume ${SERVER_NAME}-volume:${SERVER_HOME} \
        --publish 8080:${SERVER_UI_PORT} \
        --publish 50000:${SERVER_AGENT_PORT} \
        --env "JAVA_OPTS=${JAVA_OPTS}" \
        --env "JENKINS_OPTS=${JENKINS_OPTS}" \
        ${SERVER_NAME}-image:latest
fi

waitUntilServerIsReady ${SERVER_NAME}-container

# obtain cli jar name
CLI_PATH=${SERVER_HOME}/war/WEB-INF/lib
CLI_JAR=$(docker exec -t ${SERVER_NAME}-container ls ${CLI_PATH}/ | grep -e "^cli-.*\.jar" | tr -d '\r')

waitUntilServerIsUpdated ${SERVER_NAME}-container

# Install plugins
echo -e "\n\e[1mPreparing Jenkins server.\e[0m - Installing necessary plugins"
docker exec -t ${SERVER_NAME}-container \
    java -jar ${CLI_PATH}/${CLI_JAR} \
         -s http://localhost:${SERVER_UI_PORT}/ -webSocket \
         install-plugin $(cat "${TASK_DIR}/plugins.txt")

# Restart container
echo -e "\n\e[1mPreparing Jenkins server.\e[0m - Restarting"
docker exec -t ${SERVER_NAME}-container \
    java -jar ${CLI_PATH}/${CLI_JAR} \
         -s http://localhost:${SERVER_UI_PORT}/ -webSocket \
         safe-shutdown

docker start ${SERVER_NAME}-container
waitUntilServerIsReady ${SERVER_NAME}-container
waitUntilServerIsUpdated ${SERVER_NAME}-container
sleep 20 # more time before create jobs

# CREATING AND BUILDING JOB ========================================================================
# Create job
echo -e "\n\e[1mCreating and building job.\e[0m - Restarting"
export JENKINSFILE_CONTENT=$(cat "${TASK_DIR}/Jenkinsfile")
envsubst < job_template.xml > tmp
docker cp tmp ${SERVER_NAME}-container:${SERVER_HOME}/job.xml
rm tmp
docker exec -t ${SERVER_NAME}-container \
    sh -c "java -jar ${CLI_PATH}/${CLI_JAR} \
         -s http://localhost:${SERVER_UI_PORT}/ -webSocket \
         create-job \"${JOB_NAME}\" < ${SERVER_HOME}/job.xml"

# Build job
docker exec -t ${SERVER_NAME}-container \
    java -jar ${CLI_PATH}/${CLI_JAR} \
         -s http://localhost:${SERVER_UI_PORT}/ -webSocket \
         build "${JOB_NAME}"

# CHECK RESULT =====================================================================================
echo -e "\n\e[1mCheck the status of building job in the link below.\e[0m\n"
echo -e "\n\thttp://localhost:${SERVER_UI_PORT}/job/${JOB_NAME}/\n\n"
