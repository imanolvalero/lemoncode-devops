#!/bin/bash
IFS=$'\n'

TIMESTAMP=$(date "+%Y%m%d%H%M%S")
REPO_URL=https://github.com/Lemoncode/bootcamp-devops-lemoncode
DOCKER_USERNAME=imanolvalero

FRONTEND_REPO_DIR=02-orquestacion/exercises/02-distributed/todo-front
FRONTEND_NAME=lc-todo-distributed-frontend
FRONTEND_PORT=80
FRONTEND_NODE_ENV=production

BACKEND_REPO_DIR=02-orquestacion/exercises/02-distributed/todo-api
BACKEND_NAME=lc-todo-distributed-backend
BACKEND_PORT=3000
BACKEND_NODE_ENV=production
BACKEND_TESTING_IP=$(ip route get 8.8.8.8 | grep src | awk '{ print $7 }')

E2E_IMG=cypress/browsers:latest
E2E_NAME=lc-todo-distributed-e2e

SCRIPT_NAME=$(basename ${0} | sed 's/\(.*\)\..*/\1/')

function cloneGitSubfolder(){
    local repo="$1"
    local rdir="$2"
    local dest="$3"
    local cdir="${PWD}"
    local temp=$(mktemp -d -t cloneGitSubFolder.XXXXX)
    
    cd ${temp}
    git init &> /dev/null
    git remote add -f origin ${repo} &> /dev/null
    git config core.sparseCheckout true &> /dev/null
    echo "${rdir}" >> .git/info/sparse-checkout
    git pull origin master &> /dev/null
    cd ${cdir}
    rsync -r -f "-x .git" ${temp}/${rdir}/* ${dest}
    rm -Rf ${temp}
    return 0
}

# BUILD FRONTEND IMAGE FOR TESTING ====================================================
# Stop container if it is running
[ $(docker ps -a | grep -c ${FRONTEND_NAME}-test) -eq 1 ] && \
    docker stop ${FRONTEND_NAME}-test

# Remove image if exists
[ $(docker images --format "{{.Repository}}:{{.Tag}}" | grep -c ${DOCKER_USERNAME}/${FRONTEND_NAME}:latest) -ne 0 ] \
    && docker rmi -f ${DOCKER_USERNAME}/${FRONTEND_NAME}

# Clone folder and build image
[[ -e front ]] && rm -Rf front
echo -e "\e[1mGit clone\e[0m ${REPO_URL}/tree/master/${FRONTEND_REPO_DIR}"
cloneGitSubfolder "${REPO_URL}" "${FRONTEND_REPO_DIR}" "${PWD}/front"
cd front

# PATCH
# add .babelrc file
cat << 'EOF' > .babelrc
{
    "presets": [
        "@babel/preset-env",
        "@babel/preset-react",
        "@babel/preset-typescript"
    ]
}
EOF

docker build . \
    --tag ${DOCKER_USERNAME}/${FRONTEND_NAME}:latest --compress \
    --build-arg API_HOST=http://${BACKEND_TESTING_IP}:${BACKEND_PORT}
cd ..

# Launch container with new image for testing
docker run --detach --rm \
    --publish ${FRONTEND_PORT}:${FRONTEND_PORT} \
    --name ${FRONTEND_NAME}-test \
    ${DOCKER_USERNAME}/${FRONTEND_NAME}:latest


# BUILD BACKEND IMAGE ================================================================
# Stop container if it is running
[ $(docker ps -a | grep -c ${BACKEND_NAME}-test) -eq 1 ] && \
    docker stop ${BACKEND_NAME}-test

# Remove image if exists
[ $(docker images --format "{{.Repository}}:{{.Tag}}" | grep -c ${DOCKER_USERNAME}/${BACKEND_NAME}:latest) -ne 0 ] \
    && docker rmi -f ${DOCKER_USERNAME}/${BACKEND_NAME}

# Clone folder and build image
[[ -e back ]] && rm -Rf back
echo -e "\e[1mGit clone\e[0m ${REPO_URL}/tree/master/${BACKEND_REPO_DIR}"
cloneGitSubfolder "${REPO_URL}" "${BACKEND_REPO_DIR}" "${PWD}/back"
cd back
docker build . --tag ${DOCKER_USERNAME}/${BACKEND_NAME}:latest --compress
cd ..

# Launch container with new image for testing
docker run --detach --rm \
    --name ${BACKEND_NAME}-test \
    --publish ${BACKEND_PORT}:${BACKEND_PORT} \
    --env NODE_ENV=${BACKEND_NODE_ENV} \
    --env PORT=${BACKEND_PORT} \
    ${DOCKER_USERNAME}/${BACKEND_NAME}:latest



# END2END TEST ================================================================
echo -ne "\n\n\e[33mStarting e2e test... \e[0m\n"

# Cypres end-to-end test spec
mkdir -p e2e/cypress/integration
mkdir -p e2e/cypress/plugins
mkdir -p e2e/cypress/support

cat << EOF > e2e/cypress/integration/spec.js
describe('Check if website is working correctly', () => {
    beforeEach(() => {
        cy.visit('http://${BACKEND_TESTING_IP}:${FRONTEND_PORT}')
    });
    const options = { timeout: 10 * 1000 }

    it('Check main title', () => {
        const title = 'Todos App'
        cy.get('h3').should('have.text', title)
    })

    it('Add a ToDo task', () => {
        const task = 'Task 1'
        cy.get('input.MuiInputBase-input')
            .type(task, options)
        cy.get('div#root > div > div > button')
            .click(options)
        cy.get('ul li > div > div:nth-child(2) span')
            .should('have.length', 1)
            .should('have.text', task)
    })

    it('Remove a ToDo task', () => {
        cy.get('ul li > div:nth-child(2) button')
            .click(options)
        cy.get('ul li > div > div:nth-child(2) span')
            .should('have.length', 0)
    })
})
EOF

# Cypres config file
cat << EOF > e2e/cypress.json
{
    "baseUrl": null,
    "watchForFileChanges":false
}
EOF

# Launch tests
CMD="npx cypress run --headless --browser chrome"
CMD=(bash -c "${CMD}")
docker run -t --rm \
    --name ${E2E_NAME} \
    --volume ${PWD}/e2e:/app \
    --workdir /app \
    --env CYPRESS_VIDEO=false \
    ${E2E_IMG} "${CMD[@]}"

# Check if test have failed
[[ -e e2e/cypress/screenshots/spec.js ]] && exit

# Stop Backend and Frontend
docker stop -t 0 ${BACKEND_NAME}-test
docker stop -t 0 ${FRONTEND_NAME}-test


## BUILD AND PUBLISH PRODUCTION IMAGES
# Frontend
# It is necesary to rebuild frontend image for production because
# value for API_HOST build-arg is only setted for testing enviroment
cd front
docker build . \
    --tag ${DOCKER_USERNAME}/${FRONTEND_NAME}:latest --compress \
    --build-arg API_HOST=""
cd ..
docker push ${DOCKER_USERNAME}/${FRONTEND_NAME}:latest

# Backend
docker push ${DOCKER_USERNAME}/${BACKEND_NAME}:latest

## Clean 
rm -Rf e2e front back
docker rmi ${DOCKER_USERNAME}/${FRONTEND_NAME}:latest
docker rmi ${DOCKER_USERNAME}/${BACKEND_NAME}:latest


# DEPLOY ON KUBERNETES =============================================================================
kubectl apply -f .
exit

