# Modulo 3 - Kubernetes - Laboratorio

### Ejercicio 1. Monolito en memoria.
[[Ir a la carpeta]](./modulo3-kubernetes/lab/01-monolito_en_memoria)

Se trata de una aplicación web que expone una aplicación de UI (la típica aplicación TODO), y una API para gestionar en servidor los 'TODOS'. La persistencia de los TODOS es en memoria, eso significa que cuando apaguemos la aplicación dejarán de persistir los datos. 

### Ejercicio 2. Monolito
[[Ir a la carpeta]](./modulo3-kubernetes/lab/02-monolito)

Se trata de la misma aplicación pero en este caso la persistencia se hace en una base de datos PostgreSQL.

### Ejercicio 3. Aplicación Distribuida
[[Ir a la carpeta]](./modulo3-kubernetes/lab/03-distribuido)

See trata de dos aplicaciones, una UI expuesta a tarvés de un `nginx` y una `API` que corre sobre `express/nodejs`.