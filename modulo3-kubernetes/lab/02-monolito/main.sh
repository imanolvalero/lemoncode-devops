#!/bin/bash
IFS=$'\n'

TIMESTAMP=$(date "+%Y%m%d%H%M%S")
REPO_URL=https://github.com/Lemoncode/bootcamp-devops-lemoncode
REPO_DIR=02-orquestacion/exercises/01-monolith/todo-app
DOCKER_USERNAME=imanolvalero
APP_NAME=lc-todo-monolith
APP_PORT=3000
APP_NODE_ENV=production
APP_TEST_NAME=${APP_NAME}-test

SCRIPT_NAME=$(basename ${0} | sed 's/\(.*\)\..*/\1/')

function cloneGitSubfolder(){
    local repo="$1"
    local rdir="$2"
    local dest="$3"
    local cdir="${PWD}"
    local temp=$(mktemp -d -t cloneGitSubFolder.XXXXX)
    
    cd ${temp}
    git init &> /dev/null
    git remote add -f origin ${repo} &> /dev/null
    git config core.sparseCheckout true &> /dev/null
    echo "${rdir}" >> .git/info/sparse-checkout
    git pull origin master &> /dev/null
    cd ${cdir}
    rsync -r -f "-x .git" ${temp}/${rdir}/* ${dest}
    rm -Rf ${temp}
    return 0
}

function simpleTest(){
    local name="${1:-"Simple test"}"
    local desc="${2:-""}"
    local url=${3:-http://localhost}
    local text="${4:-""}"
    local delay="${5:-0}"
    local custom="${6:-""}"
    
    echo -e "\e[1mSimple Test: ${name}\e[0m\n$desc"
    [[ ${delay} -gt 0 ]] && (
        local bar=""
        local count=0
        while [ ${count} -lt ${delay} ]; do
            bar=""            
            local i=0
            while [ ${i} -lt ${delay} ]; do
                [[ ${i} -gt count ]] && bar="${bar} " || bar="${bar}>"
                (( i++ ))
            done
            echo -ne "\e[33mWaiting [${bar}] \e[0m\r"
            (( count++ ))
            sleep 1
        done
    )
    echo -ne "\e[33mTesting...\e[0m                                          \r"
    local cmd="curl -s ${url}"
    [[ -n "${custom}" ]] && cmd="${custom}"
    [[ $(eval ${cmd} | grep -c "${text}") -eq 1 ]] \
        && (echo -e "\e[32mTest passed\e[0m\n" && return 0) \
        || (echo -e "\e[31mTest failed\e[0m\n" && return 1)
}

# BUILD TEST AND REGISTER APPLICATION IMAGE ========================================================
# Stop container if it is running
[ $(docker ps -a | grep -c ${APP_NAME}) -eq 1 ] && \
    docker stop ${APP_NAME}

# Clone folder
echo -e "\e[1mGit clone\e[0m ${REPO_URL}/tree/master/${REPO_DIR}"
cloneGitSubfolder "${REPO_URL}" "${REPO_DIR}" "${PWD}/tmp"

# Remove image if exists
[ $(docker images --format "{{.Repository}}:{{.Tag}}" | grep -c ${APP_NAME}:latest) -ne 0 ] \
    && docker rmi -f ${APP_NAME}
cd tmp
docker build . --tag ${DOCKER_USERNAME}/${APP_NAME}:latest --compress
cd ..

# Launch container
docker run --detach --rm \
    --name ${APP_NAME}-test \
    ${DOCKER_USERNAME}/${APP_NAME}:latest

# Test
CMD="docker exec -t ${APP_NAME}-test wget -qO- http://localhost:3000"
simpleTest "Application is running" \
    "Application is serving index.html on port 3000" \
    "" 'id="root"' 5 "${CMD}"
TMP=$?

[[ ${TMP} -ne 0 ]] \
    && docker stop ${APP_NAME}-test > /dev/null \
    && exit ${TMP}
unset CMD

docker stop -t 0 ${APP_NAME}-test
rm -Rf tmp

# Publish
docker push ${DOCKER_USERNAME}/${APP_NAME}:latest

# Clean
docker image rm ${DOCKER_USERNAME}/${APP_NAME}:latest

# DEPLOY ON KUBERNETES =============================================================================
kubectl apply -f .

exit

